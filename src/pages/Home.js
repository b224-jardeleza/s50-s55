import { Fragment } from 'react';
import { Container } from 'react-bootstrap';

import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home() {
	return (

		<>
			<Container>
				<Banner />
				<Highlights />
			</Container>
		{/*	<Container>
				<CourseCard />
			</Container> */}
		</>
		

	)
}

/* end of s50 */