import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';


export default function Register() {

	const { user, setUser } = useContext(UserContext);
	
	const navigate = useNavigate()

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	console.log(email)
	console.log(password1)
	console.log(password2)

	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Please provide a different email."
				})

			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(input => {
					console.log(input);

					if (input === true) {
						Swal.fire({
							title: "Registration successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate('/login')

					} else {
						Swal.fire({
							title: "Registration failed!",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})


		// Clear the input fields and states
		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword1("");
		setPassword2("");

		// alert("Thank you for registering.")
	}

	useEffect(() => {
			if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo !== "") && (password1 === password2) && mobileNo.length === 11) {
				setIsActive(true)
			} else {
				setIsActive(false)
			}

			
		}, [email, password1, password2])

	return (

		(user.id !== null) ?
			<Navigate to="/courses" />
			:
			<Form onSubmit={e => registerUser(e)}>
			      <Form.Group className="mb-3" controlId="userFirstName">
			      	<Form.Label>First Name</Form.Label>
			      	<Form.Control
			      		type="text" 
			      		placeholder="Enter first name"
			      		value={firstName}
			      		onChange={e => setFirstName(e.target.value)}
			      		required
			      	/>
			      	</Form.Group>	

			      	<Form.Group className="mb-3" controlId="userLastName">
			      	<Form.Label>Last Name</Form.Label>
			      	<Form.Control
			      		type="text" 
			      		placeholder="Enter first name"
			      		value={lastName}
			      		onChange={e => setLastName(e.target.value)}
			      		required
			      	/>
			      	</Form.Group>

			      	<Form.Group className="mb-3" controlId="userEmail">  		
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email"
			        	value={email}
			        	onChange={e => setEmail(e.target.value)}
			        	required
			        />
			      	</Form.Group>

			      	<Form.Group className="mb-3" controlId="userMobileNo">  		
			        <Form.Label>Mobile No</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter mobile number"
			        	value={mobileNo}
			        	onChange={e => setMobileNo(e.target.value)}
			        	required
			        />
			      	</Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password"
				        	value={password1}
				        	onChange={e => setPassword1(e.target.value)}
				        	required
				        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Verify Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Verify Password"
				        	value={password2}
				        	onChange={e => setPassword2(e.target.value)}
				        	required
				        />
				      </Form.Group>


				      {
				      	isActive ?
				      		<Button variant="primary" type="submit" id="submitBtn">
				      		  Submit
				      		</Button>
				      		:
				      		<Button variant="primary" type="submit" id="submitBtn" disabled>
				      		  Submit
				      		</Button>
				      }
				    </Form>
	)
}