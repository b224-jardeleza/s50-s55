import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link }  from 'react-router-dom';

// export default function CourseCard(props) { // 1) using the keyword props
export default function CourseCard({courseProp}) { // 2)using attribute 

  // Checks to see if the data was successfully passed.
  // console.log(props);
  // Every component receives information in a form of object.
  // console.log(typeof props);
  // Using the dot notation, access the property to retrieve the value/data.
  // console.log(props.courseProp.name);
  // Checks if we can retrieve data from courseProp
  // console.log(courseProp)


  /*

    s51

    Use the state hook for this component to be able to store its state.
    States are used to keep track of the information related to individual components.

    Syntax:
        const [getter_variable, setter_function] = useState(initialGetterValue)

  */

  const [count, setCount] = useState(0);
  // console.log(useState(0));

  const [seats, setSeats] = useState(30);

/* --Solution for s51--
  function enroll() {
    if(seat == 0) {

      alert("No more seats.")      
    } else {

      setCount(count + 1);
      setSeat(seat - 1);
      console.log('Enrollee: ' + count);
      console.log('Seats: ' + seat);
    }

  }
*/

  function enroll() {
    if(count < 30) {
      setCount(count + 1)
      // console.log('Enrollee: ' + count)
      setSeats(seats - 1)
      // console.log('Seats: ' + seats)
    } //else {
    //   alert("No more seats available.")
    // }
  }

  // used to validate useState
  useEffect(() => {
    if(seats === 0){
      alert('No more seats available.')
    }
  }, [seats])


  // 3) using object deconstructing
  const { name, description, price, _id } = courseProp;

  return (

    <Row>
      <Col>
            <Card className="cardHighlight2 p-3">
                  <Card.Body>
                    <Card.Title>{name}</Card.Title>
        {/*         <Card.Title>{props.courseProp.name}</Card.Title>
                    <Card.Title>{courseProp.name}</Card.Title>*/}
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>Php {price}</Card.Text>
                    <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                  </Card.Body>
            </Card>
      </Col>
    </Row>


  )
}

/* end of s50 */